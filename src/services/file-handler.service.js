const multer = require('multer')
const path = require('path')
const api = require('../tools/common')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/opl/')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + (Math.random() * (999 - 100) + 100).toFixed(0) + '' + Date.now() + path.extname(file.originalname))
    }
});

const fileFilter = (req, file, cb) => {
    // Allowed file types
  const allowedTypes = [
    'application/pdf',
    'image/jpeg',
    'image/jpg',
    'image/png',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  ];

  if (allowedTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(new Error('File format is not supported'), false);
  }
}

const oplUpload = multer({
    storage: storage,
    fileFilter: fileFilter
})

module.exports = {
    oplUpload
}