const express = require('express');
const router = express.Router();
const path = require('path');

const AuthRoutes = require('./utility_routes/auth.routes')
const MasterRoutes = require('./master_routes/master.routes')
const FileRoutes = require('./utility_routes/files.routes')
const PublicRoutes = require('./utility_routes/public.routes')
const AuthMiddleware = require('../middleware/AuthMiddleware')

// not found route
router.get('/not-found', function(req, res) {
    res.status(404).sendFile(path.join(__dirname, '../views/not-found.html'));
});

// authentication routes usage 
router.use('/auth/', AuthRoutes);

// file routes usage
router.use('/file/', FileRoutes)

// public routes usage
router.use('/public/', PublicRoutes)

// master data routes usage 
router.use('/master/', AuthMiddleware.verifyToken, MasterRoutes);

module.exports = router;