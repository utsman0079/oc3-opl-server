const express = require('express');
const router = express.Router();

const OplController = require('../../controller/master_controller/OplController')

router.get('/opl/:filename', OplController.getOplFile)

module.exports = router;