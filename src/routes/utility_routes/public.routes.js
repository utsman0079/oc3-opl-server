const express = require('express');
const router = express.Router();

const AreaController = require('../../controller/master_controller/AreaController')
const MachineController = require('../../controller/master_controller/MachineController')
const OplController = require('../../controller/master_controller/OplController')
const LineController = require('../../controller/master_controller/LineController')

const FileService = require('../../services/file-handler.service')
const FileMiddleware = require('../../middleware/FileMiddleware')

// Line
router.get('/line', LineController.getAllLine)
router.get('/line/slug/:slug', LineController.getLineBySlug)

// Area
router.get('/area', AreaController.getAllArea)
router.get('/area/slug/:slug', AreaController.getAreaBySlug)
router.get('/area/line/:lineId', AreaController.getAreaByLineId)

// Machine
router.get('/machine', MachineController.getAllMachine)
router.get('/machine/search-area/:areaId', MachineController.searchMachinePerArea)
router.get('/machine/slug/:slug', MachineController.getMachineBySlug)
router.get('/machine/area/:areaId', MachineController.getMachineByAreaId)

// OPL
router.get('/opl/machine/:machineId', OplController.getOplByMachineIdAndDate)
router.get('/opl/pagination/machine/:slug', OplController.searchOplPaginationByMachine)
router.get('/opl/pagination/area/:slug', OplController.searchOplPaginationByArea)
router.post('/opl', OplController.insertOpl)
router.post('/opl/upload', FileService.oplUpload.single("file"), FileMiddleware.handleUploadError, OplController.uploadOplFile)
router.put('/opl/:id', OplController.updateOpl)

module.exports = router;