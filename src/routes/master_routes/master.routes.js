const express = require('express');
const MasterMiddleware = require('../../middleware/MasterMiddleware')
const FileMiddleware = require('../../middleware/FileMiddleware')
const router = express.Router();

const LineController = require('../../controller/master_controller/LineController')
const AreaController = require('../../controller/master_controller/AreaController')
const MachineController = require('../../controller/master_controller/MachineController')
const OplController = require('../../controller/master_controller/OplController')
const UserController = require('../../controller/master_controller/UserController')

const FileService = require('../../services/file-handler.service')

// Factory Line
router.get('/line', LineController.getAllLine)
router.get('/line/paging', LineController.searchByPagination)
router.get('/line/:id', LineController.getLineById)
router.post('/line', LineController.insertLine)
router.put('/line/:id', LineController.updateLine)

// Area
router.get('/area', AreaController.getAllArea)
router.get('/area/paging', AreaController.searchAreaByPagination)
router.get('/area/:id', AreaController.getAreaById)
router.get('/area/line/:lineId', AreaController.getAreaByLineId)
router.post('/area', AreaController.insertArea)
router.put('/area/:id', AreaController.updateArea)

// Machine
router.get('/machine', MachineController.getAllMachine)
router.get('/machine/paging', MachineController.searchMachineByPagination)
router.get('/machine/:id', MachineController.getMachineById)
router.get('/machine/slug/:slug', MachineController.getMachineBySlug)
router.get('/machine/area/:areaId', MachineController.getMachineByAreaId)
router.post('/machine', MachineController.insertMachine)
router.put('/machine/:id', MachineController.updateMachine)

// OPL
router.get('/opl/machine/:machineId', OplController.getOplByMachineIdAndDate)
router.get('/opl/pagination/area/:slug', OplController.searchOplPaginationByArea)
router.get('/opl/pagination/machine/:slug', OplController.searchOplPaginationByMachine)
router.post('/opl', OplController.insertOpl)
router.post('/opl/upload', FileService.oplUpload.single("file"), FileMiddleware.handleUploadError, OplController.uploadOplFile)
router.put('/opl/:id', OplController.updateOpl)

// Users
router.get('/users', UserController.getAllUsers)
router.get('/users/paging', UserController.searchUserByPagination)
router.get('/users/role', UserController.getUserRole)
router.get('/users/:id', UserController.getUserByNik)
router.post('/users', MasterMiddleware.checkUserInsert, UserController.insertUser)
router.put('/users/:id', MasterMiddleware.checkUserUpdate, UserController.updateUser)
router.delete('/users/:id', UserController.deleteUser)

module.exports = router;

