const model = require("../../model/area.model")
const api = require("../../tools/common")

const getAllArea = async (req, res) => {
    try {
        let data = await model.getAll()
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getAreaById = async (req, res) => {
    try {
        let data = await model.getById(req.params.id)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getAreaBySlug = async (req, res) => {
    try {
        let data = await model.getBySlug(req.params.slug)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const insertArea = async (req, res) => {
    try {
        let data = await model.insert(req.body.form_data)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateArea = async (req, res) => {
    try {
        let data = await model.update(req.params.id, req.body.form_data)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const searchAreaByPagination = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 25;
        const offset = (page - 1) * pageSize;
        const sortColumn = req.query.sortColumn || ''
        const sortDirection = req.query.sortDirection || ''
        const term = req.query.search || ''
        const data = await model.searchPagination(term, offset, pageSize, sortColumn, sortDirection)
        const total = await model.getSearchLength(term)

        return res.json({ status: true, total: total, data: data })
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getAreaByLineId = async (req, res) => {
    try {
        let data = await model.getByLineId(req.params.lineId)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

module.exports = {
    getAllArea,
    getAreaById,
    getAreaBySlug,
    insertArea,
    updateArea,
    searchAreaByPagination,
    getAreaByLineId
}