const model = require("../../model/opl.model")
const api = require("../../tools/common")
const path = require('path')
const fs = require('fs');

const insertOpl = async (req, res) => {
    try {
        let request = req.body.form_data
        if (request && request.created_at === null) {
            delete request['created_at']
        }
        let data = await model.insert(request)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateOpl = async (req, res) => {
    try {
        let request = req.body.form_data
        if (request && request.created_at === null) {
            delete request['created_at']
        }
        let data = await model.update(req.params.id, request)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getOplByMachineIdAndDate = async (req, res) => {
    try {
        const machineId = req.params.machineId

        const today = new Date()
        let _90DaysAgo = new Date()
        _90DaysAgo.setDate(today.getDate() - 90)

        const fromDate = req.query.from || _90DaysAgo.toISOString().slice(0, 10)
        const toDate = req.query.to || today.toISOString().slice(0,10)

        let data = await model.getAllByMachineIdAndDate(machineId, fromDate, toDate)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const searchOplPaginationByArea = async (req, res) => {
    try {
        const area = req.params.slug || ''
        const fromDate = req.query.from || ''
        const toDate = req.query.to || ''

        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 25;
        const offset = (page - 1) * pageSize;
        const sortColumn = req.query.sortColumn || ''
        const sortDirection = req.query.sortDirection || ''
        const term = req.query.search || '';

        const data = await model.searchPaginationByAreaAndDate(
            term, offset, pageSize, sortColumn, sortDirection, area, fromDate, toDate
        )
        const total = await model.getSearchLengthByAreaAndDate(term, area, fromDate, toDate)

        return res.json({ status: true, total: total, data: data})
    } catch (err) {
        return api.catchError(res, err)
    }
}

const searchOplPaginationByMachine = async (req, res) => {
    try {
        const machine = req.params.slug || ''
        const fromDate = req.query.from || ''
        const toDate = req.query.to || ''

        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 25;
        const offset = (page - 1) * pageSize;
        const sortColumn = req.query.sortColumn || ''
        const sortDirection = req.query.sortDirection || ''
        const term = req.query.search || '';

        const data = await model.searchPaginationByMachineAndDate(
            term, offset, pageSize, sortColumn, sortDirection, machine, fromDate, toDate
        )
        const total = await model.getSearchLengthByMachineAndDate(term, machine, fromDate, toDate)

        return res.json({ status: true, total: total, data: data})
    } catch (err) {
        return api.catchError(res, err)
    }
}

const uploadOplFile = async (req, res) => {
    try {
        let fileName = req.file.filename
        let fileSize = parseInt(req.file.size) || 0
        let data = { file_name: fileName, file_size: fileSize }
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getOplFile = async (req, res) => {
    const filename = req.params.filename || ''
    const filePath = path.join(__dirname, `../../../uploads/opl/${filename}`)
    let newFileName = req.query.name || filename
    
    if (path.extname(filename) !== '.pptx') {
        return res.sendFile(filePath)
    }

    newFileName = ensureFileExtension(filename, newFileName)

    fs.access(filePath, fs.constants.F_OK, (err) => {
        if (err) {
            return api.error(res, `No such file or directory`, 400)
        } else {
            res.download(filePath, newFileName, (_err) => {
                if (_err) {
                    return api.catchError(res, _err)
                }
            })
        }
    })
}

function ensureFileExtension(originalFilename, newFilename) {
    const originalExtension = path.extname(originalFilename);
    const newExtension = path.extname(newFilename);
    if (!newExtension) {
        return newFilename + originalExtension;
    }
    return newFilename;
}

module.exports = {
    insertOpl,
    updateOpl,
    getOplByMachineIdAndDate,
    uploadOplFile,
    searchOplPaginationByArea,
    searchOplPaginationByMachine,
    getOplFile
}
