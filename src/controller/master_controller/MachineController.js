const model = require("../../model/machine.model")
const api = require("../../tools/common")

const getAllMachine = async (req, res) => {
    try {
        let data = await model.getAll()
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getMachineById = async (req, res) => {
    try {
        let data = await model.getById(req.params.id)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getMachineBySlug = async (req, res) => {
    try {
        let data = await model.getBySlug(req.params.slug)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const insertMachine = async (req, res) => {
    try {
        let data = await model.insert(req.body.form_data)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const updateMachine = async (req, res) => {
    try {
        let data = await model.update(req.params.id, req.body.form_data)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const searchMachinePerArea = async (req, res) => {
    try {
        let areaId = req.params.areaId
        let term = req.query.search
        let data = await model.searchPerArea(areaId, term)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

const searchMachineByPagination = async (req, res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 25;
        const offset = (page - 1) * pageSize;
        const sortColumn = req.query.sortColumn || ''
        const sortDirection = req.query.sortDirection || ''
        const term = req.query.search || ''
        const data = await model.searchPagination(term, offset, pageSize, sortColumn, sortDirection)
        const total = await model.getSearchLength(term)

        return res.json({ status: true, total: total, data: data })
    } catch (err) {
        return api.catchError(res, err)
    }
}

const getMachineByAreaId = async (req, res) => {
    try {
        let data = await model.getByAreaId(req.params.areaId)
        return api.ok(res, data)
    } catch (err) {
        return api.catchError(res, err)
    }
}

module.exports = {
    getAllMachine,
    getMachineById,
    getMachineBySlug,
    insertMachine,
    updateMachine,
    searchMachinePerArea,
    searchMachineByPagination,
    getMachineByAreaId
}