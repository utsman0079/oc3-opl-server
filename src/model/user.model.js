const db = require("../database/opl.config");

const getAll = async () => await db.select("*").from("v_users");

const getByNik = async (nik) =>
  await db.select("*").from("v_users").where("nik", nik);

const getById = async (id) =>
  await db.select("*").from("v_users").where("user_id", id);

const insert = async (data) => await db("mst_user").insert(data);

const update = async (id, data) =>
  await db("mst_user").where("id", id).update(data);

const deleteData = async (id) => await db("mst_user").where("id", id).delete()

const getRole = async () => await db.select("*").from("mst_user_role");

const searchPagination = async (term, offset, pageSize, sortCol, sortDir) =>
  await db("v_users")
    .select("*")
    .where(builder => {
      builder.where("nik", "like", `%${term}%`)
      .orWhere("name", "like", `%${term}%`)
      .orWhere("email", "like", `%${term}%`)
      .orWhere("role_name", "like", `%${term}%`)
    })
    .orderBy(sortCol ? sortCol : 'created_at', sortDir ? sortDir : 'asc')
    .offset(offset)
    .limit(pageSize);

const getSearchLength = async (term) =>
  await db("v_users")
  .count("user_id", { as: 'total' })
  .where(builder => {
    builder.where("nik", "like", `%${term}%`)
    .orWhere("name", "like", `%${term}%`)
    .orWhere("email", "like", `%${term}%`)
    .orWhere("role_name", "like", `%${term}%`)
  }).then(data => +data[0].total)

module.exports = {
  getAll,
  getByNik,
  getById,
  insert,
  update,
  deleteData,
  getRole,
  searchPagination,
  getSearchLength,
};
