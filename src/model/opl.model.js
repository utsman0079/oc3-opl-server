const db = require('../database/opl.config')

const insert = async (data) => await db("tr_opl").insert(data)
const update = async (id, data) => await db("tr_opl").where("id", id).update(data)

const getAllByMachineIdAndDate = async (machineId, fromDate, toDate) => {
    return await db("v_opl")
        .where("machine_id", machineId)
        .andWhereBetween("date_added", [`${fromDate}`, `${toDate}`])
}

const searchPaginationByAreaAndDate = async (term, offset, pageSize, sortCol, sortDir, areaSlug, fromDate, toDate) => {
    let baseQuery = db("v_opl")
        .where("area_slug", areaSlug)
        .where(builder => {
            builder.where("detail", "like", `%${term}%`)
                .orWhere("machine", "like", `%${term}%`)
                .orWhere("date_added", "like", `%${term}%`)
                .orWhere("uploaded_by", "like", `%${term}%`)
                .orWhere("file_ext", "like", `%${term}%`)
        })
    
    if (fromDate && toDate) {
        baseQuery = baseQuery.andWhereBetween("date_added", [`${fromDate}`, `${toDate}`])
    }

    return await baseQuery
       .orderBy(sortCol ? sortCol : 'created_at', sortDir ? sortDir : 'desc')
       .offset(offset)
       .limit(pageSize)

}

const getSearchLengthByAreaAndDate = async (term, areaSlug, fromDate, toDate) => {
    let baseQuery = db("v_opl")
        .count("opl_id", { as: 'total' })
        .where("area_slug", areaSlug)
        .where(builder => {
            builder.where("detail", "like", `%${term}%`)
                .orWhere("machine", "like", `%${term}%`)
                .orWhere("date_added", "like", `%${term}%`)
                .orWhere("uploaded_by", "like", `%${term}%`)
                .orWhere("file_ext", "like", `%${term}%`)
        })

        if (fromDate && toDate) {
            baseQuery = baseQuery.andWhereBetween("date_added", [`${fromDate}`, `${toDate}`])
        }

    return await baseQuery.then(data => +data[0].total)
}

const searchPaginationByMachineAndDate = async (term, offset, pageSize, sortCol, sortDir, machineSlug, fromDate, toDate) => {
    let baseQuery = db("v_opl")
        .where("machine_slug", machineSlug)
        .where(builder => {
            builder.where("detail", "like", `%${term}%`)
                .orWhere("machine", "like", `%${term}%`)
                .orWhere("date_added", "like", `%${term}%`)
                .orWhere("uploaded_by", "like", `%${term}%`)
        })
    
    if (fromDate && toDate) {
        baseQuery = baseQuery.andWhereBetween("date_added", [`${fromDate}`, `${toDate}`])
    }

    return await baseQuery
       .orderBy(sortCol ? sortCol : 'created_at', sortDir ? sortDir : 'desc')
       .offset(offset)
       .limit(pageSize)

}

const getSearchLengthByMachineAndDate = async (term, machineSlug, fromDate, toDate) => {
    let baseQuery = db("v_opl")
        .count("opl_id", { as: 'total' })
        .where("machine_slug", machineSlug)
        .where(builder => {
            builder.where("detail", "like", `%${term}%`)
                .orWhere("machine", "like", `%${term}%`)
                .orWhere("date_added", "like", `%${term}%`)
                .orWhere("uploaded_by", "like", `%${term}%`)
        })

        if (fromDate && toDate) {
            baseQuery = baseQuery.andWhereBetween("date_added", [`${fromDate}`, `${toDate}`])
        }

    return await baseQuery.then(data => +data[0].total)
}

module.exports = {
    insert,
    update,
    getAllByMachineIdAndDate,
    searchPaginationByAreaAndDate,
    getSearchLengthByAreaAndDate,
    searchPaginationByMachineAndDate,
    getSearchLengthByMachineAndDate
}