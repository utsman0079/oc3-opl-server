const db = require("../database/opl.config");

const getAll = async () => await db.select("*").from("v_area")
const getById = async (id) => await db.select("*").from("v_area").where("id", id)
const insert = async (data) => await db("mst_area").insert(data)
const update = async (id, data) => await db("mst_area").where("id", id).update(data)

const getBySlug = async (slug) => await db.select("*").from("v_area").where("slug", slug)

const searchPagination = async (term, offset, pageSize, sortCol, sortDir) => {
    return await db
        .select("*")
        .from("v_area")
        .where(builder => {
            builder.where("name", "like", `%${term}%`)
            .orWhere("slug", "like", `%${term}%`)
            .orWhere("detail", "like", `%${term}%`)
            .orWhere("line", "like", `%${term}%`)
        })
        .orderBy(sortCol ? sortCol : 'id', sortDir ? sortDir : 'asc')
        .offset(offset)
        .limit(pageSize);
}

const getSearchLength = async (term) => {
    return await db
        .count("id", { as: "total" })
        .from("v_area")
        .where(builder => {
            builder.where("name", "like", `%${term}%`)
            .orWhere("slug", "like", `%${term}%`)
            .orWhere("detail", "like", `%${term}%`)
            .orWhere("line", "like", `%${term}%`)
        })
        .then(data => +data[0].total)
}

const getByLineId = async (lineId) => await db.select("*").from("v_area").where("line_id", lineId)

module.exports = {
    getAll,
    getById,
    insert,
    update,
    getBySlug,
    searchPagination,
    getSearchLength,
    getByLineId
}
