const db = require("../database/opl.config");

const login = async (nik) =>
  await db.select("*").from("v_users").where("nik", nik).groupBy("nik");

module.exports = {
  login,
};
