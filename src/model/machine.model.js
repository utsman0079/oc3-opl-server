const db = require("../database/opl.config");

const getAll = async () => {
    return await db.select("mma.*", "ma.name as area")
        .from("mst_machine_area as mma")
        .leftJoin("mst_area as ma", "mma.area_id", "ma.id")
        .where("mma.is_removed", 0)
}
const getById = async (id) => await db.select("*").from("mst_machine_area").where("is_removed", 0).andWhere("id", id)
const insert = async (data) => await db("mst_machine_area").insert(data)
const update = async (id, data) => await db("mst_machine_area").where("id", id).update(data)

const getBySlug = async (slug) => {
    return await db("mst_machine_area as mma")
        .select("mma.*", "ma.slug as area_slug", "mfl.slug as line_slug")
        .leftJoin("mst_area as ma", "mma.area_id", "ma.id")
        .leftJoin("mst_factory_line as mfl", "ma.line_id", "mfl.id")
        .where("mma.slug", slug)
        .where("mma.is_removed", 0)
}

const searchPerArea = async (areaId, term) => {
    return await db("mst_machine_area")
        .where("area_id", areaId)
        .where("is_removed", 0)
        .andWhere(builder => {
            builder.where("name", "like", `%${term}%`)
        })
        .limit(10)
}

const searchPagination = async (term, offset, pageSize, sortCol, sortDir) => {
    return await db
        .select("mfl.name as line", "mfl.id as line_id", "ma.name as area", "mma.*")
        .from("mst_machine_area as mma")
        .leftJoin("mst_area as ma", "mma.area_id", "ma.id")
        .leftJoin("mst_factory_line as mfl", "ma.line_id", "mfl.id")
        .where("mma.is_removed", 0)
        .andWhere(builder => {
            builder.where("mma.name", "like", `%${term}%`)
            .orWhere("mma.detail", "like", `%${term}%`)
            .orWhere("ma.name", "like", `%${term}%`)
            .orWhere("mfl.name", "like", `%${term}%`)
        })
        .orderBy(sortCol ? sortCol : 'mma.id', sortDir ? sortDir : 'asc')
        .offset(offset)
        .limit(pageSize);
}

const getSearchLength = async (term) => {
    return await db
        .count("mma.id", { as: "total" })
        .from("mst_machine_area as mma")
        .leftJoin("mst_area as ma", "mma.area_id", "ma.id")
        .leftJoin("mst_factory_line as mfl", "ma.line_id", "mfl.id")
        .where("mma.is_removed", 0)
        .andWhere(builder => {
            builder.where("mma.name", "like", `%${term}%`)
            .orWhere("mma.detail", `%${term}%`)
            .orWhere("ma.name", "like", `%${term}%`)
            .orWhere("mfl.name", "like", `%${term}%`)
        })
        .then(data => +data[0].total)
}

const getByAreaId = async (areaId) => await db.select("*").from("mst_machine_area").where("is_removed", 0).andWhere("area_id", areaId)

module.exports = {
    getAll,
    getById,
    getBySlug,
    insert,
    update,
    searchPerArea,
    searchPagination,
    getSearchLength,
    getByAreaId
}
