const multer = require('multer');
const api = require('../tools/common')

const handleUploadError = (err, req, res, next) => {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      return api.error(res, `Multer error: ${err.message}`, 400)
    } else {
      // An unknown error occurred when uploading.
      return api.catchError(res, err, 400)
    }
  }

module.exports = {
    handleUploadError
}