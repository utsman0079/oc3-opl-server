const api = require('../tools/common')
const userModel = require('../model/user.model')

const checkUserInsert = async (req, res, next) => {
    return insertCheckUniqueVal(
        'nik', userModel.getByNik, 
        "NIK is already exists",
        req, res, next
    )
}

const checkUserUpdate = async (req, res, next) => {
    return updateCheckUniqueVal(
        'nik', userModel.getById, userModel.getByNik,
        "NIK is already exists",
        req, res, next
    )
}

async function insertCheckUniqueVal(uniqueKey, modelFn, existMsg, req, res, next) {
    try {
        const data = await modelFn(req.body.form_data[uniqueKey])
        if (data.length > 0) {
            return api.error(res, existMsg, 400)
        }
        next()
    } catch (err) {
        api.catchError(res, err)
    }
}

async function updateCheckUniqueVal(uniqueKey, modelFnId, modelFnUnique, existMsg, req, res, next) {
    try {
        const dataId = req.params.id
        const dataUnique = req.body.form_data[uniqueKey]

        if (!dataUnique) {
            next()
        } else {
            const currentData = await modelFnId(dataId)
            const updateData = await modelFnUnique(dataUnique)
            if (updateData.length == 1) {
                if (updateData[0][uniqueKey] === currentData[0][uniqueKey]) {
                    next()
                } else {
                    return api.error(res, existMsg, 400)
                }
            } else {
                next()
            }
        }

    } catch (err) {
        api.catchError(res, err)
    }
}

module.exports = {
    checkUserInsert,
    checkUserUpdate
}